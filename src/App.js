import { useState, useEffect } from "react";
import "./App.css";
import LoginPage from "./components/Login/LoginPage";
import RegistrationPage from "./components/Register/RegistrationPage";
import Home from "./components/Home/home";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import axios from "axios";
import ProtectedRoutes from "./components/Home/ProtectedRoutes";
import AddCustomer from "./components/AddCustomers/AddCustomer";
import CustomerDashboard from "./components/CustomerDashboard/CustomerDashboard";
import "antd/dist/antd.min.css";
import {googleLogout} from '@react-oauth/google';


const App = () => {
  const [loggedInUser, getLoggedInUser] = useState("");
  const [loggedInSessionUser, getLoggedInSessionUser] = useState("");

  const resFromServer = (response) => {
    if (response) {
      getLoggedInUser(response);
    }
  };

  //removing details not cookie, check with it again
  const logoutUser = () => {
    //Logging out from google oauth
    googleLogout();
    axios
      .get("http://localhost:8080/clearUserSession")
      .then(() => {
        getLoggedInUser("");
        getLoggedInSessionUser("");
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    axios.defaults.withCredentials = true;
    axios
      .get("http://localhost:8080/auth")
      .then((res) => {
        getLoggedInSessionUser(res.data.user);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [loggedInUser]);

  return (
    <Router>
      <Routes>
        <Route
          exact
          path="/"
          element={
            <ProtectedRoutes
              Component={Home}
              loggedInUser={loggedInUser}
              loggedInSessionUser={loggedInSessionUser}
              loggedInUserDetails={
                loggedInSessionUser._id
                  ? loggedInSessionUser
                  : loggedInUser
              }
              logoutUser={logoutUser}
            />
          }
        />
        <Route exact path="/addcustomers" element={
          <ProtectedRoutes 
            Component={AddCustomer}
            loggedInUser={loggedInUser}
            loggedInSessionUser={loggedInSessionUser}
            loggedInUserDetails={
              loggedInSessionUser._id
                ? loggedInSessionUser
                : loggedInUser
            }
            logoutUser={logoutUser}
          />
        }/>

        <Route exact path="/customerdashboard" element={
          <ProtectedRoutes 
            Component={CustomerDashboard}
            loggedInUser={loggedInUser}
            loggedInSessionUser={loggedInSessionUser}
            loggedInUserDetails={
              loggedInSessionUser._id
                ? loggedInSessionUser
                : loggedInUser
            }
            logoutUser={logoutUser}
          />
        }/>

        <Route
          path="/login"
          element={<LoginPage loggedInUser={loggedInUser} loggedInSessionUser={loggedInSessionUser} resFromServer={resFromServer}/>}
        />
        <Route path="/register" element={<RegistrationPage />} />
      </Routes>
    </Router>
  );
};

export default App;
