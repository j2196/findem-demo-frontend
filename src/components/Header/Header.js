import React from "react";
import findemLogo from "../images/findem-logo.jpg";
import "./header.styles.css";

import {
  DownOutlined
} from "@ant-design/icons";
import {Layout, Menu, Image, Row, Col, Dropdown, Space, Typography } from "antd";
const { Header } = Layout;

const defaultHeader = ({loggedInUserDetails,logoutUser}) => (
  <>
    <Layout>
        <Header className="header" style={{background: "#fff"}}>
          <Row justify="space-between">
            <Col span={15}><Image width={100} src={findemLogo} /></Col>
            <Col span={3}>
              <a href="/">Home</a>
            </Col>
            <Col span={6}>
              <Dropdown overlay={(
                <Menu items={[
                  {
                    key:"Profile",
                    label:"Profile"
                  },
                  {
                    key:"Settings",
                    label:"Settings"
                  },
                  {
                    key:"Logout",
                    label:"Logout",
                    onClick: () => logoutUser()
                  },
                ]} />
                )}>
                <Typography.Link>
                  <Space>
                    {`Hello ${loggedInUserDetails.name}`}
                    <DownOutlined />
                  </Space>
                </Typography.Link>
              </Dropdown>
            </Col>
          </Row>
        </Header>
      </Layout>
  </>
);

export default defaultHeader;
