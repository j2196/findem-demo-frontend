import React, {useState} from "react";

const FilterComponent = ({dataOriginal, dataCloned, filteredCustomersData }) => {
  const[filterCustomers, getFilteredCustomers] = useState("")

  if (dataOriginal) {
    //includes duplicates
    let customersWithDuplicates = dataOriginal.map((object) => object.customer)

    let customers = customersWithDuplicates.filter((customer, index) => customersWithDuplicates.indexOf(customer) === index);

    const getFilterOptions = () => {

      if(filterCustomers !== "") {
        customers = customers.filter((customer) => customer.includes(filterCustomers))
      } 

      let getFilterCustomerOptions = customers.map((customer, index) => {
        return (
          <div key={index} className="dropdown-item">
            <input id={index} className="me-2" style={{cursor:'pointer'}} value={customer} type="checkbox"></input>
            <label htmlFor={index} style={{cursor:'pointer'}}>{customer}</label>
          </div>
        );
      });

        return getFilterCustomerOptions;

      }

    const handleFilterOptions = (event) => {
      event.preventDefault();
      const selectedCustomers = customers.filter((customer, index) => event.target[index + 1].checked);
      //calling home component function, there i am changing the whole data state as per requirement
      filteredCustomersData(selectedCustomers);
    }

    const handleInputChange = (event) => {
      getFilteredCustomers(event.target.value)
    }

    return (
      <div className="dropdown" style={{cursor:"pointer"}}>
        <p className="text-end mb-1 dropdown-toggle p-1" id="dropdownMenuClickableInside" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-expanded="false">
            <span style={{ color: "grey" }}>Filter by Customer</span>{" "}
            <i className="fa-solid fa-filter ms-2"></i>
        </p>
        <div className="dropdown-menu" style={{width:"100%"}} aria-labelledby="dropdownMenuClickableInside">
          <div className={`d-flex justify-content-center`}>
            <form onSubmit={handleFilterOptions} >
              <div className="input-icons">
                <i className="fa-solid fa-magnifying-glass icon"></i>
                <input className="filter-search input-field" id="search" onChange={handleInputChange} type="text" placeholder="search customer"/>
              </div>
              <div style={{overflowY:"scroll", height:"6rem"}}>
                {getFilterOptions()}
              </div>
              <div className="mt-3 mb-2 d-flex justify-content-between">
                <button className="btn btn-secondary me-2" type="reset">reset</button>
                <button className="btn btn-primary" type="submit">search</button>
              </div>
            </form>
          </div>
        </div>
        <hr />
      </div>
    );
  }
};

export default FilterComponent;
