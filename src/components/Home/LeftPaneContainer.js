import React from 'react';

const LeftPaneContainer = ({displayUsers, displayDetailsOnRightPane, selectedLeftPane}) => {
  if(displayUsers) {
     let leftPaneContainers = displayUsers.map((object, index) => {
      return (
        <div key={index} className="left-pane-container">
          <div
            id={index}
            onClick={displayDetailsOnRightPane}
            className="p-3 d-flex justify-content-between left-pane-container-items"
            style={{backgroundColor: Number(selectedLeftPane) === Number(index) ? "rgb(236, 236, 236)" : "white"}}
          >
            <div id={index}>
              <h6 id={index}>
                <strong id={index}>{object.name}</strong>
              </h6>
              <p id={index} style={{ fontStyle: "italic", color: "grey", margin:"0" }}>
                Shortlisted {object.shortlistedOn}
              </p>
            </div>
            <div id={index}>
              <h6 id={index}>
                <strong id={index}>{object.customer}</strong>
              </h6>
            </div>
          </div>
          <hr/>
        </div>
      );
    })
     
    return leftPaneContainers;
  } 
  
}

export default LeftPaneContainer;

