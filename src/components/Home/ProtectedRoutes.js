import React from "react";
import { Navigate } from "react-router-dom";

function ProtectedRoutes(props) {

  const {
    Component,
    loggedInSessionUser,
    loggedInUser
  } = props;
  if (loggedInSessionUser.name || loggedInUser.name) {
    return (
      <Component {...props}/>
    );
  } else {
    return <Navigate to="/login" />;
  }
  //     if(rest.loggedInUser.name || rest.loggedInSessionUser.name) {
  //     return(
  //         <Component {...rest}/>
  //     )
  //   } else {
  //       return (
  //         <Navigate to="/login"/>
  //       )
  //   }
}

export default ProtectedRoutes;
