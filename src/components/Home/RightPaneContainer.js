import React, { useEffect, useState } from "react";
import "./rightPaneContainerStyles.css"

const RightPaneContainer = ({ clickedUserDetails: user }) => {
  const [newEmail, setNewEmail] = useState("");
  const [newPhone, setNewPhone] = useState("");
  const [newLinkedIn, setNewLinkedIn] = useState("");

  useEffect(() => {
    setNewEmail(user.email);
    setNewPhone(user.phone);
    setNewLinkedIn(user.linkedIn);
  }, [user.email, user.phone, user.linkedIn])

  const handleFormSubmit = (event) => {
    event.preventDefault();
    console.log("form submitted")
    console.log(event)
  }

  const handleDeleteInput = (event) => {
    if(event.target.id === 'email') {
      setNewEmail("");
    }
    else if(event.target.id === 'phone') {
      setNewPhone("");
    } 
    else if (event.target.id === 'linkedin') {
      setNewLinkedIn("");
    }
  }

  const handleInputChange = (event) => {
    if(event.target.name === 'email') {
      setNewEmail(event.target.value)
    }
    else if(event.target.name === 'phone') {
      setNewPhone(event.target.value)
    }
    else if(event.target.name === 'linkedIn') {
      setNewLinkedIn(event.target.value)
    }
  }

  if (user) {
    return (
      <div>
        <div className="d-flex justify-content-between p-4">
          <div className="d-flex flex-column">
            <h6>
              <strong>{user.name}</strong>
            </h6>
            <p style={{ color: "grey" }}>{user.currentDesignation}</p>
            <a href="https://www.findem.ai/" target="blank">
              Findem Enriched Profile{" "}
              <i className="fa-solid fa-arrow-up-right-from-square ms-1"></i>
            </a>
          </div>
          <div>
            <h6 style={{ textAlign: "end" }}>
              <strong>Customer: {user.customer}</strong>
            </h6>
            <h6 style={{ textAlign: "end" }}>
              <strong>Shortlisted on: {user.shortlistedOn}</strong>
            </h6>
          </div>
        </div>
        <hr></hr>
        {/* Form  */}
        <form className="p-4 mt-4" onSubmit={handleFormSubmit}>
          <div>
              <div className="d-flex">
                <label className="me-5"><i className="fa-solid fa-envelope me-1"></i>Email: </label>
                <div className="d-flex align-items-baseline">
                  <input className="input-styles" type="email" placeholder="Enter Email" value={newEmail} name="email" onChange={handleInputChange}/>  
                  <i className="fa-solid fa-circle-xmark" style={{cursor:"pointer"}} data-bs-toggle="modal" data-bs-target="#email"></i>
                </div>
              </div>

              <div className="mt-5">
                <div className="d-flex">
                  <label className="me-5"><i className="fa-solid fa-phone me-1"></i>Phone: </label>
                  <div className="d-flex align-items-baseline">
                    <input className="input-styles" type="number" placeholder="Enter Phone" name="phone" value={newPhone} onChange={handleInputChange}/>  
                    <i className="fa-solid fa-circle-xmark" style={{cursor:"pointer"}} data-bs-toggle="modal" data-bs-target="#phone"></i>
                  </div>
                </div>
                <p style={{marginLeft:"7rem", marginTop:"1rem"}}><a href="/">Add phone</a></p>
              </div>

              <div className="d-flex mt-5">
                <label className="me-5"><i className="fa-brands fa-linkedin me-1"></i>LinkedIn: </label>
                <div className="d-flex align-items-baseline">
                  <input className="input-styles" type="email" placeholder="Enter LinkedIn Profile Url" value={newLinkedIn} name="linkedIn" onChange={handleInputChange}/>  
                  <i className="fa-solid fa-circle-xmark" style={{cursor:"pointer"}} data-bs-toggle="modal" data-bs-target="#linkedin"></i>
                </div>
              </div>
            </div>

            {/* Email Deletion Modal */}
            <div className="modal fade" id="email" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-hidden="true">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body p-5 text-center">
                    <p>Are you sure you want to delete email?</p>
                    <hr/>
                    <div className="d-flex justify-content-end mt-3">
                      <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">No</button>
                      <button type="button" id="email" data-bs-dismiss="modal" className="btn btn-primary ms-2" onClick={handleDeleteInput}>Yes</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* Phone Deletion Modal */}
            <div className="modal fade" id="phone" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-hidden="true">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body p-5 text-center">
                    <p>Are you sure you want to delete phone?</p>
                    <hr/>
                    <div className="d-flex justify-content-end mt-3">
                      <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">No</button>
                      <button type="button" id="phone" data-bs-dismiss="modal" className="btn btn-primary ms-2" onClick={handleDeleteInput}>Yes</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* LinkedIn Deletion Modal */}
            <div className="modal fade" id="linkedin" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-hidden="true">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body p-5 text-center">
                    <p>Are you sure you want to delete linkedIn url?</p>
                    <hr/>
                    <div className="d-flex justify-content-end mt-3">
                      <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">No</button>
                      <button type="button" id="linkedin" data-bs-dismiss="modal" className="btn btn-primary ms-2" onClick={handleDeleteInput}>Yes</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="d-flex justify-content-end p-4">
                <button type="button" className="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#formModal">Save</button>
            </div>

            {/* Form Submission Modal */}
            <div className="modal fade" id="formModal" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="formModalLabel" aria-hidden="true">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body p-5 text-center">
                    <p>Are you sure you want to save changes?</p>
                    <hr/>
                    <div className="d-flex justify-content-end mt-3">
                      <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">No</button>
                      <button type="submit" className="btn btn-primary ms-2">Yes</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>


        </form>
      </div>
    );
  }
};

export default RightPaneContainer;



