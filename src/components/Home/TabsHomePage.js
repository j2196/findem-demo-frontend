import React, { useState } from "react";
import { Link } from "react-router-dom";


const TabsHomePage = () => {
  const [activePageId, setActivePageId] = useState("tab1");

  const handleActivePage = (event) => {
    setActivePageId(event.target.id);
  }

  return (
    <>
      <ul className="nav nav-tabs">
        <li className="nav-item">
          <Link to="/" id="tab1" onClick={handleActivePage} className={`nav-link ${activePageId === "tab1" ? "active" : ""}`}>
            Add Missing Emails
          </Link>
        </li>
        <li className="nav-item">
          <Link to="/addcustomers" onClick={handleActivePage} id="tab2" className={`nav-link ${activePageId === "tab2" ? "active" : ""}`}>
            Add Customers
          </Link>
        </li>
        <li className="nav-item">
          <Link to="/customerdashboard" onClick={handleActivePage} id="tab3" className={`nav-link ${activePageId === "tab3" ? "active" : ""}`}>
            customer dashboard
          </Link>
        </li>
      </ul>
    </>
  );
};

export default TabsHomePage;
