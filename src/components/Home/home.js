import React, { useEffect, useState, useRef } from "react";
import "./home.styles.css";
import Header from "../Header/Header";
import "./home.styles.css"
import FilterComponent from "./FilterComponent";
import LeftPaneContainer from "./LeftPaneContainer";
import RightPaneContainer from "./RightPaneContainer";
import TabsHomePage from "./TabsHomePage";
import axios from "axios";
import moment from 'moment';
import { Spin } from 'antd';

const Homepage = ({loggedInUserDetails, logoutUser}) => {
  const [data, getData] = useState("");
  const [dataInAction, setDataInAction] = useState("");
  const [selectedLeftPane, selectLeftPane] = useState("white");
  const [isLeftPaneClicked, handleLeftPaneClick] = useState(false);
  const [clickedUserDetails, getClickedUserDetails] = useState("");
  const [pageCount, setPageCount] = useState(10);
  const buttonRef = useRef(null);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    const fetchUsers = async () => {
      const res = await axios.get("http://localhost:8080/api/v1/candidates/getData");

      //sorting data in ascending order of shortlisted dates
      let sortedData = res.data.candidates.sort((preObj, currObj) => {
        //changing string into date format using moment and comparing
        let date1 = moment(preObj.shortlistedOn, "DD/MM/YYYY").toDate();
        let date2 = moment(currObj.shortlistedOn,"DD/MM/YYYY").toDate();
        
        return date1 - date2;
      });

      getData(sortedData);
      setDataInAction(sortedData);
    };

    fetchUsers();
  }, []);

  let displayUsers = dataInAction.slice(0, pageCount);

  const displayDetailsOnRightPane = (event) => {
    let requiredObject = data[event.target.id];
    selectLeftPane(event.target.id);
    handleLeftPaneClick(true);
    getClickedUserDetails(requiredObject);
  }

  const filteredCustomersData = (filteredCustomers) => {
    //if some customers were selected only the rendering the required users data
    if(filteredCustomers.length !== 0) {
      let changedData = data.filter((object) => {
        return filteredCustomers.includes(object.customer)
      });
      setDataInAction(changedData);
    } else {
      setDataInAction(data);
    }
  }

  const loadMoreData = () => {
    setPageCount((preValue) => preValue + pageCount)
  }

  const toggleVisible = (event) => {
    // console.log('scrollTop: ', event.currentTarget.scrollTop);
    // console.log('offsetHeight: ', event.currentTarget.offsetHeight);
    if (event.currentTarget.scrollTop > 300){
    setVisible(true)
    }
    else if (event.currentTarget.scrollTop <= 300){
    setVisible(false)
    }
  };
  
  const scrollToTop = () =>{
    buttonRef.current.scrollTo({
    top: 0,
    behavior: 'smooth'
    });
  };

  if(data) {
    return (
      <div>
        <Header loggedInUserDetails={loggedInUserDetails} logoutUser={logoutUser}/>
        <h3 className="text-center mt-5"><span style={{color: "#4798FE", fontWeight:"600"}}>FINDEM</span> DATA RESEARCH APPLICATION</h3>
        <div className="container mt-5">
          <TabsHomePage />
        </div>
        <div className="container mt-4 total-data-container">
          <div className="row"> 
            <div className='col-4 p-3' style={{overflowY:"scroll", height:"60vh"}} id="scrollContainer" ref={buttonRef} onScroll={toggleVisible}>
              <FilterComponent dataOriginal={data} dataCloned={dataInAction} filteredCustomersData={filteredCustomersData}/>
              <LeftPaneContainer onClick={handleLeftPaneClick} displayUsers={displayUsers} displayDetailsOnRightPane={displayDetailsOnRightPane} selectedLeftPane={selectedLeftPane}/>
              <div className='d-flex justify-content-center mt-3'>
                <button className='btn btn-primary' onClick={loadMoreData}>Load More</button>
              </div>
              {/* Scroll Button */}
              <div className="d-flex justify-content-end">
                <button className="scroll-top-button" onClick={scrollToTop} style={{display: visible ? 'inline' : 'none'}}><i className="fa-solid fa-circle-arrow-up scroll-top-icon"></i></button>
              </div>
            </div>
            <div className="col-8">
              {
                isLeftPaneClicked 
                ?
                  <RightPaneContainer clickedUserDetails={clickedUserDetails}/>
                :
                <div style={{height:"100%"}} className="d-flex justify-content-center align-items-center">
                  <div>
                    <p style={{color:"grey"}}><i className="fa-solid fa-arrow-pointer me-1"></i>Select left pane to display the details</p>
                  </div>
                </div>
              }
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div className="d-flex justify-content-center align-items-center" style={{height:"97vh"}}>
        <Spin />
      </div>

    )
  }
};

export default Homepage;
