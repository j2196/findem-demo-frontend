import React, { Component } from "react";
import validator from "validator";
import axios from 'axios';
import {Link} from 'react-router-dom';

class RegistrationPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      nameError: "",
      userEmail: "",
      userEmailError: "",
      role: "",
      roleError: "",
      userPassword: "",
      userPasswordError: "",
      confirmPassword: "",
      confirmPasswordError: "",
      termsCheckboxError: "",
      togglePasswordType: "password",
      eyeIconDisplayPassword: "none",
      eyeSlashIconDisplayPassword: "in-line",
      toggleConfirmPasswordType: "password",
      eyeIconDisplayConfirmPassword: "none",
      eyeSlashIconDisplayConfirmPassword: "in-line",
      isFormSubmitted: false,
      registerErrorMessages: "",
      registerErrorsDisplayAlert: "none",
    };
  }

  handleInputChange = (event) => {
    if (event.target.name === "name") {
      if (!validator.isAlpha(event.target.value)) {
        this.setState({
          name: event.target.value,
          nameError: "Name should contain only letters",
        });
      } else {
        this.setState({
          name: event.target.value,
          nameError: "",
        });
      }
    } else if (event.target.name === "email") {
      if (!validator.isEmail(event.target.value)) {
        this.setState({
          userEmail: event.target.value,
          userEmailError: "Please enter a valid email address",
        });
      } else {
        this.setState({
          userEmail: event.target.value,
          userEmailError: "",
        });
      }
    } else if(event.target.name === 'role') {
        if(!event.target.value) {
          this.setState({
            role: event.target.value,
            roleError: "Please select a role"
          })
        } else {
            this.setState({
              role: event.target.value,
              roleError: ""
            })
        }
    } else if (event.target.name === "password") {
      if (event.target.value.length < 0) {
        this.setState({
          userPasswordError: "Please enter a valid password",
        });
      } else {
        this.setState({
          userPassword: event.target.value,
          userPasswordError: "",
        });
      }
    } else if (event.target.name === "confirmPassword") {
      if (event.target.value.length < 0) {
        this.setState({
          confirmPasswordError: "Please enter a valid password",
        });
      } else if (event.target.value !== this.state.userPassword) {
        this.setState({
          confirmPassword: event.target.value,
          confirmPasswordError: "Password do not match",
        });
      } else {
        this.setState({
          confirmPassword: event.target.value,
          confirmPasswordError: "",
        });
      }
    } else if (event.target.name === "checkbox") {
      if (event.target.checked) {
        this.setState({
          termsCheckboxError: "",
        });
      } else {
        this.setState({
          termsCheckboxError: "Please agree to the terms and conditions",
        });
      }
    }
  };

  handleFormSubmission = (event) => {
    event.preventDefault();
    if (!validator.isAlpha(event.target[0].value)) {
      this.setState({
        nameError: "Name should contain only letters",
      });
    } else {
      this.setState({
        nameError: "",
      });
    }

    if (!validator.isEmail(event.target[1].value)) {
      this.setState({
        userEmailError: "Please enter a valid email address",
      });
    } else {
      this.setState({
        userEmailError: "",
      });
    }
  
    if (!event.target[2].value) {
      this.setState({
        roleError: "Please select a role",
      });
    } else {
      this.setState({
        roleError: "",
      });
    }

    if (event.target[3].value.length === 0) {
      this.setState({
        userPasswordError: "Please enter a valid password",
      });
    } else {
      this.setState({
        userPasswordError: "",
      });
    }

    if (event.target[4].value.length === 0) {
      this.setState({
        confirmPasswordError: "Please enter a valid password",
      });
    } else if (event.target[3].value !== event.target[4].value) {
      this.setState({
        confirmPasswordError: "Passwords do not match",
      });
    } else {
      this.setState({
        confirmPasswordError: "",
      });
    }

    if (!event.target[5].checked) {
      this.setState({
        termsCheckboxError: "Please agree to the terms and conditions",
      });
    } else {
      this.setState({
        termsCheckboxError: "",
      });
    }

    if (
      this.state.name &&
      this.state.userEmail &&
      this.state.role &&
      this.state.userPassword &&
      this.state.confirmPassword &&
      !this.state.nameError &&
      !this.state.userEmailError &&
      !this.state.roleError &&
      !this.state.userPasswordError &&
      !this.state.confirmPasswordError &&
      !this.state.termsCheckboxError
    ) {
      let data = {
        name: this.state.name,
        email: this.state.userEmail,
        role: this.state.role,
        password: this.state.userPassword,
      }
      axios.post('http://localhost:8080/register', data)
        .then((res) => {
    
          if(!res.data.user) {
             this.setState({
              isFormSubmitted: false,
              registerErrorMessages: res.data.message,
              registerErrorsDisplayAlert: "block"
             })
          } else {
            this.setState({
              isFormSubmitted: true,
              registerErrorMessages: "",
              registerErrorsDisplayAlert: "none"
            })
          }
        })
        .catch((error) => {
          console.log(error)
        })
    
    }
  };

  showPassword = (event) => {
    if (event.target.id === "password") {
      this.setState({
        togglePasswordType: "text",
        eyeIconDisplayPassword: "in-line",
        eyeSlashIconDisplayPassword: "none",
      });
    } else {
      this.setState({
        toggleConfirmPasswordType: "text",
        eyeIconDisplayConfirmPassword: "in-line",
        eyeSlashIconDisplayConfirmPassword: "none",
      });
    }
  };

  hidePassword = (event) => {
    if (event.target.id === "password") {
      this.setState({
        togglePasswordType: "password",
        eyeIconDisplayPassword: "none",
        eyeSlashIconDisplayPassword: "in-line",
      });
    } else {
      this.setState({
        toggleConfirmPasswordType: "password",
        eyeIconDisplayConfirmPassword: "none",
        eyeSlashIconDisplayConfirmPassword: "in-line",
      });
    }
  };


  render() {
    
    if(this.state.isFormSubmitted) {
      return(
        <div className="modal d-block bg-secondary" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div className="modal-body text-center">
                <i className="fa-solid fa-circle-check text-success" style={{fontSize: "2rem"}}></i>
                <p style={{fontSize: "1rem"}}>Registration Successful!</p>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" onClick={() => window.location.reload()}>Close</button>
                <Link to="/login"><button type="button" className="btn btn-primary">Login</button></Link>
              </div>
            </div>
          </div>
        </div>
      )
    } else {
      return (
        <div >
          {/* Register Error Messages */}
            <div className={`alert alert-danger alert-dismissible fade show text-center d-${this.state.registerErrorsDisplayAlert}`} role="alert">
                {this.state.registerErrorMessages}
                <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" onClick={() => {this.setState({registerErrorsDisplayAlert: 'none', registerErrorMessages: ""})}}></button>
            </div>
          <div className="container bg-container">
            <div className="row mt-5">
              <div className="col-6 carousel-container p-1">
                <div
                  id="carouselExampleIndicators"
                  className="carousel slide w-100 carousel-alignment h-25"
                  data-bs-ride="carousel"
                >
                  <div className="carousel-inner mb-5 h-100 text-center">
                    <div className="carousel-item active w-100">
                      
                      <h4>What is Lorem Ipsum</h4>
                      <p>
                          There are many variations of passages of Lorem Ipsum
                          available.
                      </p>
                    </div>
                    <div className="carousel-item w-100">
                        <h4>Why do we use it</h4>
                        <p>
                          There are many variations of passages of Lorem Ipsum
                          available.
                        </p>
                    </div>
                    <div className="carousel-item w-100">
                        <h4>Where can I get some</h4>
                        <p>
                          There are many variations of passages of Lorem Ipsum
                          available.
                        </p>
          
                    </div>
                  </div>
                  
                  <div className="carousel-indicators">
                    <button
                      type="button"
                      data-bs-target="#carouselExampleIndicators"
                      data-bs-slide-to="0"
                      className="active"
                      aria-current="true"
                      aria-label="Slide 1"
                    ></button>
                    <button
                      type="button"
                      data-bs-target="#carouselExampleIndicators"
                      data-bs-slide-to="1"
                      aria-label="Slide 2"
                    ></button>
                    <button
                      type="button"
                      data-bs-target="#carouselExampleIndicators"
                      data-bs-slide-to="2"
                      aria-label="Slide 3"
                    ></button>
                  </div>
  
                </div>
              </div>
              <div className="col-6">
                <p className="mt-3">Have an Account? <Link to="/login">Login</Link></p>
                <form
                  onSubmit={this.handleFormSubmission}
                  noValidate="noValidate"
                  className="p-4"
                  id="registerForm"
                >
                  <h4>REGISTRATION</h4>
  
                  <div className="mb-1">
                    <label htmlFor="name" className="form-label">
                      Name
                    </label>
                    <input
                      type="text"
                      name="name"
                      className="form-control"
                      id="name"
                      value={this.state.name}
                      onChange={this.handleInputChange}
                    />
                    <p className="text-danger">{this.state.nameError}</p>
                  </div>
  
                  <div className="mb-1">
                    <label htmlFor="userEmail" className="form-label">
                      Email address
                    </label>
                    <input
                      type="email"
                      name="email"
                      className="form-control"
                      id="userEmail"
                      value={this.state.userEmail}
                      onChange={this.handleInputChange}
                    />
                    <p className="text-danger">{this.state.userEmailError}</p>
                  </div>

                  <div className="mb-1"> 
                    <label htmlFor="userRole" className="form-label">
                      Role
                    </label>
                      <select form="registerForm" name="role" id="userRole" value={this.state.role} onChange={this.handleInputChange} className="form-select" aria-label="Default select example">
                        <option value=""></option>
                        <option value="Dev">Dev</option>
                        <option value="Testing">Testing</option>
                        <option value="Networking">Networking</option>
                        <option value="HR & Payroll">HR & Payroll</option>
                        <option value="L&D">L&D</option>
                        <option value="Marketing">Marketing</option>
                      </select>
                      <p className="text-danger">{this.state.roleError}</p>
                  </div>
  
                  <div className="mb-1">
                    <label htmlFor="userPassword" className="form-label">
                      Password
                    </label>
                    <div className="d-flex justify-content-between align-items-baseline">
                      <input
                        type={this.state.togglePasswordType}
                        name="password"
                        className="form-control"
                        autoComplete="on"
                        id="userPassword"
                        value={this.state.userPassword}
                        onChange={this.handleInputChange}
                      />
                      <div className="d-flex justify-content-between">
                        <i
                          className={`fa-solid fa-eye d-${this.state.eyeIconDisplayPassword}`}
                          id="password"
                          onClick={this.hidePassword}
                        ></i>
                        <i
                          className={`fa-solid fa-eye-slash d-${this.state.eyeSlashIconDisplayPassword}`}
                          id="password"
                          onClick={this.showPassword}
                        ></i>
                      </div>
                    </div>
                    <p className="text-danger">{this.state.userPasswordError}</p>
                  </div>
  
                  <div className="mb-1">
                    <label htmlFor="userPassword" className="form-label">
                      Confirm Password
                    </label>
                    <div className="d-flex justify-content-between align-items-baseline">
                      <input
                        type={this.state.toggleConfirmPasswordType}
                        name="confirmPassword"
                        className="form-control"
                        autoComplete="on"
                        id="confirmPassword"
                        value={this.state.confirmPassword}
                        onChange={this.handleInputChange}
                      />
                      <div className="d-flex justify-content-between">
                        <i
                          className={`fa-solid fa-eye d-${this.state.eyeIconDisplayConfirmPassword}`}
                          id="confirmPassword"
                          onClick={this.hidePassword}
                        ></i>
                        <i
                          className={`fa-solid fa-eye-slash d-${this.state.eyeSlashIconDisplayConfirmPassword}`}
                          id="confirmPassword"
                          onClick={this.showPassword}
                        ></i>
                      </div>
                    </div>
                    <p className="text-danger">
                      {this.state.confirmPasswordError}
                    </p>
                  </div>
  
                  <div className="mb-1 form-check">
                    <input
                      type="checkbox"
                      name="checkbox"
                      onChange={this.handleInputChange}
                      className="form-check-input"
                      id="checkboxTandC"
                    />
                    <label className="form-check-label" htmlFor="checkboxTandC">
                      I have read and agree to the{" "}
                      <a href="terms">Terms & conditions</a>
                    </label>
                    <p className="text-danger">{this.state.termsCheckboxError}</p>
                  </div>

                  <button type="submit" className="btn btn-primary mt-1">
                    Register
                  </button>
                  
                </form>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}

export default RegistrationPage;
